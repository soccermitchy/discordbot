﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace discordbot
{
    public class UserHandler : IUserHandler
    {
        private UserList Users { get; set; }
        public UserHandler()
        {
            if (!File.Exists("users.json"))
            {
                Users = new UserList();
                Save();
            }
            Users = UserList.FromJson(File.ReadAllText("users.json"));
        }

        public void Save()
        {
            var jsonData = Users.ToJson();
            File.WriteAllText("users.json", jsonData);
        }

        public User GetUser(ulong id)
        {
            //return Users.Users.FirstOrDefault(u => u.Id == id);
            var userQuery = Users.Users.Where(u => u.Id == id);
            var userList = userQuery.ToList();
            if (userList.Any()) return userList.First();
            var user = new User() {Id = id, Level = 0};
            Users.Users.Add(user);
            return user;

        }
        public int GetLevel(ulong id)
        {
            return GetUser(id).Level;
        }

        public void SetLevel(ulong id, int level)
        {
            GetUser(id).Level = level;
            Save();
        }

        public UserList GetUserList()
        {
            return Users;
        }
    }
}
