﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace discordbot
{
    [AttributeUsage(AttributeTargets.Class)]
    public class PluginAttribute : Attribute
    {
    }

    public class PluginLoader
    {
        public static void Load(string path, IServiceCollection serviceCollection)
        {
            var asm = Assembly.LoadFrom(path);
            foreach (var type in asm.GetTypes())
            {
                var attribs = Attribute.GetCustomAttributes(type);
                foreach (var attrib in attribs)
                {
                    if (!(attrib is PluginAttribute)) continue;
                    var constructors = type.GetConstructors(BindingFlags.Public);
                    var useServiceColl = false;
                    foreach (var ctr in constructors) {
                        var paramList = ctr.GetParameters();
                        if (paramList.Count() == 1 && typeof(IServiceCollection).IsAssignableFrom(paramList.First().GetType()))
                            useServiceColl = true;
                    }
                    if (useServiceColl)
                        Activator.CreateInstance(type, serviceCollection);
                    else
                        Activator.CreateInstance(type);
                }
            }
        }
    }
}
