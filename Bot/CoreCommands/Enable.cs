using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace discordbot.CoreCommands
{
    [Command("enable", 100)]
    public class Enable : CommandBase
    {
        private ICommandHandler CommandHandler { get; set; }
        public Enable(ICommandHandler commandHandler) {
            CommandHandler = commandHandler;
        }
        public override async Task OnExecute(ICollection<string> arguments, SocketMessage message) {
            if (arguments.Count == 0) {
                await message.Channel.SendMessageAsync("Usage: enable [command]");
                return;
            }
            if (!CommandHandler.EnableCommand(arguments.First())) {
                await message.Channel.SendMessageAsync("Command not found");
                return;
            }
            await message.Channel.SendMessageAsync("Done");
        }
    }

}