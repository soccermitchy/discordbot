﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace discordbot.CoreCommands
{
    [Command("su")]
    public class Su : CommandBase
    {
        private IUserHandler UserHandler { get; set; }
        private ICommandHandler CommandHandler { get; set; }

        public Su(IUserHandler userHandler, ICommandHandler commandHandler)
        {
            UserHandler = userHandler;
            CommandHandler = commandHandler;
        }

        public async override Task<bool> OnLoad(Configuration cfg)
        {
            // Needed because it wants to make the .Any to .All - making it so ALL users have to have perms level 100 to disable the command
            // ReSharper disable once SimplifyLinqExpression
            return !UserHandler.GetUserList().Users.Any(u => u.Level == 100);
        }

        public override async Task OnExecute(ICollection<string> arguments, SocketMessage message)
        {
            UserHandler.SetLevel(message.Author.Id, 100);
            CommandHandler.DisableCommand("su");
            await message.Channel.SendMessageAsync(
                "Your permissions level has been set to 100, and this command has been disabled.");
        }
    }
}
