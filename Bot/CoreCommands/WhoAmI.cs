﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace discordbot.CoreCommands
{
    [Command("whoami")]
    public class WhoAmI : CommandBase
    {
        private IUserHandler UserHandler { get; set; } 
        public WhoAmI(IUserHandler userHandler)
        {
            UserHandler = userHandler;
        }
        public override async Task OnExecute(ICollection<string> arguments, SocketMessage message)
        {
            var permsLevel = UserHandler.GetLevel(message.Author.Id);
            await message.Channel.SendMessageAsync("You are " + message.Author.Username + " with permissions level " + permsLevel);
        }
    }
}
