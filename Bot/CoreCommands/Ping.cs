﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord.WebSocket;
using discordbot;

namespace discordbot.CoreCommands
{
    [Command("ping")]
    public class Ping : CommandBase
    {
        public async override Task<bool> OnLoad(Configuration cfg)
        {
            return true;
        }
        public override async Task OnExecute(ICollection<string> arguments, SocketMessage message) 
        {
            await message.Channel.SendMessageAsync("Pong");
        }
        public async override Task OnUnload() {
        
        }
    }
}
