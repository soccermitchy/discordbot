using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace discordbot.CoreCommands
{
    [Command("disable", 100)]
    public class Disable : CommandBase
    {
        private ICommandHandler CommandHandler { get; set; }
        public Disable(ICommandHandler commandHandler) {
            CommandHandler = commandHandler;
        }
        public override async Task OnExecute(ICollection<string> arguments, SocketMessage message) {
            if (arguments.Count == 0) {
                await message.Channel.SendMessageAsync("Usage: disable [command]");
                return;
            }
            if (!CommandHandler.DisableCommand(arguments.First())) {
                await message.Channel.SendMessageAsync("Command not found");
                return;
            }
            await message.Channel.SendMessageAsync("Done");
        }
    }

}