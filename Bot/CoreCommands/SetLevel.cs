using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace discordbot.CoreCommands
{
    [Command("setLevel", 100)]
    public class SetLevel : CommandBase
    {
        private IUserHandler UserHandler {get;set;}
        public SetLevel(IUserHandler userHandler) {
            UserHandler = userHandler;
        }
        public override async Task OnExecute(ICollection<string> args, SocketMessage msg) {
            if (args.Count != 2) {
                await msg.Channel.SendMessageAsync("Usage: setLevel [user] [level]");
                return;
            }
            if (msg.MentionedUsers.Count == 0) {
                await msg.Channel.SendMessageAsync("No mentioned users found!");
                return;
            }
            var user = msg.MentionedUsers.First();
            UserHandler.SetLevel(user.Id, int.Parse(args.Skip(1).First()));
            await msg.Channel.SendMessageAsync("Done");
        }
    }
}