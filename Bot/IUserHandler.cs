﻿using System;
using System.Collections.Generic;
using System.Text;

namespace discordbot
{
    public interface IUserHandler
    {
        int GetLevel(ulong id);
        void SetLevel(ulong id, int level);
        UserList GetUserList();
        User GetUser(ulong id);
        void Save();
    }
}
