﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

namespace discordbot
{
    [AttributeUsage(AttributeTargets.Class)]
    public class CommandAttribute : Attribute
    {
        public string CommandName;
        public int MinLevel;
        public bool DefaultDisabled;

        public CommandAttribute(string commandName, int minLevel = 0, bool defaultDisabled = false)
        {
            CommandName = commandName;
            MinLevel = minLevel;
            DefaultDisabled = defaultDisabled;
        }
    }

    public class CommandData
    {
        public int MinLevel {get;set;}
        public bool Disabled {get;set;}
        public CommandBase Command {get;set;}
    }
    public class Commands : ICommandHandler
    {
        private Dictionary<string, CommandData> CommandDict {get; set;} = new Dictionary<string, CommandData>();

        public async Task ScanCommands(Configuration cfg, IServiceProvider serviceProvider)
        {
            await Util.Log(new LogMessage(LogSeverity.Debug, "ScanCmds", "Scanning loaded assemblies for commands..."));

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var asm in assemblies)
            {
                foreach (var type in asm.GetTypes())
                {
                    if (!typeof(CommandBase).IsAssignableFrom(type)) continue;
                    var attribs = Attribute.GetCustomAttributes(type);
                    foreach (var attrib in attribs)
                    {
                        if (!(attrib is CommandAttribute)) continue;

                        var info = (CommandAttribute) attrib;
                        await Util.Log(new LogMessage(LogSeverity.Debug, "ScanCmds",
                            "Found command: " + info.CommandName + " | MinLevel: " + info.MinLevel +
                            " | DefaultDisabled: " + info.DefaultDisabled));

                        var cmdInstance = (CommandBase)ActivatorUtilities.CreateInstance(serviceProvider, type);
                        //var cmdInstance = (CommandBase)Activator.CreateInstance(type);
                        var success = await cmdInstance.OnLoad(cfg);
                        if (!success) {
                            await Util.Log(new LogMessage(LogSeverity.Error, "ScanCmds", "Command " + info.CommandName + " failed load! (returned false in OnLoad)"));
                            continue;
                        }
                        var cmdData = new CommandData() {
                            MinLevel = info.MinLevel,
                            Disabled = info.DefaultDisabled,
                            Command = cmdInstance
                        };
                        CommandDict.Add(info.CommandName, cmdData);
                        
                        await Util.Log(new LogMessage(LogSeverity.Info, "ScanCmds", "Command " + info.CommandName + " ready"));
                    }
                }
            }
        }

        public async Task ExecuteCommand(string name, ICollection<string> args, SocketMessage msg, IServiceProvider serviceProvider, IUserHandler userHandler) {
            if (!CommandDict.ContainsKey(name) || CommandDict[name].Disabled) {
                await msg.Channel.SendMessageAsync("discord: no such file or directory: ./"+name);
                return;
            }
            var userLevel = userHandler.GetLevel(msg.Author.Id);
            if (CommandDict[name].MinLevel > userLevel) {
                await msg.Channel.SendMessageAsync("discord: permission denied: ./" + name);
                return;
            }
            try {
                await CommandDict[name].Command.OnExecute(args, msg);
            } catch (Exception e) {
                await msg.Channel.SendMessageAsync("Segmentation fault");
                await Util.Log(new LogMessage(LogSeverity.Error, "CmdExec", "Exception while running " + msg.Content, e));
            }
            
        }

        public bool DisableCommand(string name)
        {
            if (!CommandDict.ContainsKey(name)) return false;
            CommandDict[name].Disabled = true;
            return true;
        }

        public bool EnableCommand(string name)
        {
            if (!CommandDict.ContainsKey(name)) return false;
            CommandDict[name].Disabled = false;
            return true;
        }
    }
}