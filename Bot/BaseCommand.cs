using System.Collections.Generic;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace discordbot
{
    public class CommandBase {
        public async virtual Task<bool> OnLoad(Configuration config) {
            await Util.Log(new LogMessage(LogSeverity.Debug, "cmd", "OnLoad undefined, doing nothing"));
            return true;
        }
        public async virtual Task OnExecute(ICollection<string> arguments, SocketMessage message) {
            await message.Channel.SendMessageAsync("Command not implemented! (might want to implement OnExecute)");
        }
        public async virtual Task OnUnload() {
            await Util.Log(new LogMessage(LogSeverity.Debug, "cmd", "OnUnload undefined, doing nothing"));
        }
    }
}