﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace discordbot
{
    public interface ICommandHandler
    {
        Task ScanCommands(Configuration cfg, IServiceProvider serviceProvider);
        Task ExecuteCommand(string name, ICollection<string> arguments, SocketMessage msg, IServiceProvider serviceProvider, IUserHandler userHandler);
        bool DisableCommand(string name);
        bool EnableCommand(string name);
    }
}
