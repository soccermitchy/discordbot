﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace discordbot
{
    public class UserList
    {
        [JsonProperty("users", Required = Required.Always)]
        public List<User> Users { get; set; } = new List<User>();

        public static UserList FromJson(string json) => JsonConvert.DeserializeObject<UserList>(json);
        public string ToJson() => JsonConvert.SerializeObject(this);
    }

    public class User
    {
        [JsonProperty("id", Required = Required.Always)]
        public ulong Id { get; set; }

        [JsonProperty("level", Required = Required.Always)]
        public int Level { get; set; }

        [JsonProperty("pluginData")]
        public Dictionary<string, object> PluginData { get; set; }
    }
}
