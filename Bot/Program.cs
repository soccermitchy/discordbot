﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

namespace discordbot
{


    class Program
    {
        public Configuration Config;
        public Commands Commands;
        public IServiceProvider ServiceProvider;

        static int Main(string[] args) => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task<int> MainAsync()
        {
            var client = new DiscordSocketClient();
            client.Log += Util.Log;
            if (!File.Exists("config.json"))
            {
                Console.WriteLine("Configuration not found, generating one for you...");
                Config = new Configuration()
                {
                    BotToken = "Your Discord Bot Token Here"
                };
                var cfgJson = Config.ToJson();
                File.WriteAllText("config.json", cfgJson);
                return 1;
            }

            Config = Configuration.FromJson(File.ReadAllText("config.json"));

            var token = Config.BotToken;
            await client.LoginAsync(TokenType.Bot, token);

            client.MessageReceived += MessageReceived;
            
            Commands = new Commands();

            var serviceCollection = new ServiceCollection()
                .AddSingleton<ICommandHandler>(Commands)
                .AddSingleton<IUserHandler, UserHandler>();

            foreach (var pluginPath in Directory.EnumerateFiles("Plugins", "*.dll", SearchOption.AllDirectories))
            {
                await Util.Log(new LogMessage(LogSeverity.Debug, "PluginLdr", "Loading plugin from path: " + pluginPath));
                PluginLoader.Load(pluginPath, serviceCollection);
            }
            ServiceProvider = serviceCollection.BuildServiceProvider();
            await Commands.ScanCommands(Config, ServiceProvider);
            await client.StartAsync();
            await Task.Delay(-1);

            return 0;
        }

        private async Task MessageReceived(SocketMessage msg)
        {
            await Util.Log(new LogMessage(LogSeverity.Info, "MsgRecv",
                "<" + msg.Author.Username + "#" + msg.Author.Discriminator + "> " + msg.Content));

            if (msg.Content.StartsWith(Config.Prefix))
            {
                if (msg.Content.Length == Config.Prefix.Length) return;

                var messageWithoutPrefix = msg.Content.Substring(Config.Prefix.Length);

                var splitMsg = messageWithoutPrefix.Split(" ");
                var command = splitMsg.First();
                var args = new List<string>();

                if (splitMsg.Length > 1)
                {
                    args = splitMsg.Skip(1).ToList();
                }

                await Util.Log(new LogMessage(LogSeverity.Debug, "MsgRecv",
                    "Command: " + command + " | Args: " + string.Join(" ", args)));
                await Commands.ExecuteCommand(command, args, msg, ServiceProvider, ServiceProvider.GetService<IUserHandler>());
            }
        }
    }
}
