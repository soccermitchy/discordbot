﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace discordbot
{
    public class Configuration
    {
        [JsonProperty("BotToken", Required = Required.Always)]
        public string BotToken { get; set; }

        [JsonProperty("Prefix")]
        public string Prefix { get; set; } = "./";

        //         public static Configuration FromJson(string json) => JsonConvert.DeserializeObject<Configuration>(json, discordbot.Converter.Settings);
        public static Configuration FromJson(string json) => JsonConvert.DeserializeObject<Configuration>(json);
        public string ToJson() => JsonConvert.SerializeObject(this);
    }
}
