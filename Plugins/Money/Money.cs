namespace discordbot.money {
    public class Money : IMoney
    {
        private IUserHandler UserHandler { get; set; }
        public Money(IUserHandler userHandler) {
            UserHandler = userHandler;
        }
        public MoneyStorage GetUser(ulong id) {
            var us = UserHandler.GetUser(id);
            if (!us.PluginData.ContainsKey("money")) {
                us.PluginData.Add("money", new MoneyStorage());
            }
            return (MoneyStorage)us.PluginData["money"];
        }

        public decimal AddUserMoney(ulong id, decimal amount)
        {
            var user = GetUser(id);
            user.Amount = user.Amount + amount;
            UserHandler.Save();
            return user.Amount;
        }

        public decimal GetUserMoney(ulong id)
        {
            var user = GetUser(id);
            return user.Amount;
        }

        public decimal SetUserMoney(ulong id, decimal amount)
        {
            var user = GetUser(id);
            user.Amount = amount;
            UserHandler.Save();
            return user.Amount;
        }
    }
}