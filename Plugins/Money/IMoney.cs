namespace discordbot.money {
    public interface IMoney {
        decimal GetUserMoney(ulong id);
        decimal SetUserMoney(ulong id, decimal amount);
        decimal AddUserMoney(ulong id, decimal amount);
    }
}