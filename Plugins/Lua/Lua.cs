﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using discordbot;
using Discord.WebSocket;
using MoonSharp.Interpreter;

namespace Lua
{
    [Command("lua")]
    public class Lua : CommandBase
    {
        public override async Task OnExecute(ICollection<string> arguments, SocketMessage message)
        {
            if (arguments.Count == 0)
            {
                await message.Channel.SendMessageAsync("Usage: lua [code]");
                return;
            }

            var code = string.Join(" ", arguments);
            code = code.Trim('`');
            try
            {
                const CoreModules sandboxModules = CoreModules.Basic | CoreModules.GlobalConsts | CoreModules.TableIterators |
                                                   CoreModules.String | CoreModules.Table | CoreModules.ErrorHandling | 
                                                   CoreModules.Math | CoreModules.Coroutine | CoreModules.OS_Time;
                var script = new Script(sandboxModules);
                var result = script.DoString(code);

                var resultStr = "```" + result.JankyToString();

                if (resultStr.Length > 1900)
                {
                    resultStr = resultStr.Substring(0, 1900) + "``` [response shortened to 1900 characters]";
                }
                else
                {
                    resultStr = resultStr + "```";
                }

                await message.Channel.SendMessageAsync(resultStr);
            }
            catch (InterpreterException ex)
            {
                await message.Channel.SendMessageAsync("Lua error: `" + ex.DecoratedMessage + "`");
            }
        }
    }
}
