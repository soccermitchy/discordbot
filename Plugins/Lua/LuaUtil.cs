﻿using System;
using System.Collections.Generic;
using System.Text;
using MoonSharp.Interpreter;

namespace Lua
{
    public static class LuaUtil
    {
        public static string JankyToString(this DynValue val)
        {
            var type = val.Type;
            switch (type)
                {
                    case DataType.Nil:
                        return "nil";
                    case DataType.Void:
                        return "void";
                    case DataType.Boolean:
                        return val.Boolean ? "true" : "false";
                    case DataType.Number:
                        return val.CastToString();
                    case DataType.String:
                        return val.CastToString();
                    case DataType.Function:
                        return "function";
                    case DataType.Table:
                        return "table";
                    case DataType.Tuple:
                        return "tuple";
                    case DataType.UserData:
                        return "userdata";
                    case DataType.Thread:
                        return "thread";
                    case DataType.ClrFunction:
                        return "ClrFunction";
                    case DataType.TailCallRequest:
                        return "TailCallRequest";
                    case DataType.YieldRequest:
                        return "YieldRequest";
                    default:
                        throw new ArgumentOutOfRangeException();
                }
        }
    }
}
